## Add new application

- Application:
- Chart:

### New Application Checklist

- [ ] `values.yaml` matches [one-click `values.yaml`](https://gitlab.com/gitlab-org/gitlab/-/tree/master/vendor) (if also installable via GitLab UI)
- [ ] `verify` test added
- [ ] Merge request submitted to update the [template project](https://gitlab.com/gitlab-org/project-templates/cluster-management)

### Notes

<!-- Anything else worth noting -->

/label ~feature
